public class NewContractToClause {
    
    Public List<Contract_to_Clause__c> contractToClauseList = new List<Contract_to_Clause__c>();
    public  Contract_to_Clause__c cds{get; set;}
    
    public NewContractToClause(){
           cds= new Contract_to_Clause__c();
      }
    public List <Contract_to_Clause__c> getContractToClause() {
        String cId= ApexPages.currentPage().getParameters().get('refRecord');
        contractToClauseList = [SELECT Id, Name,Contract__c,Clauses__c 
                                FROM Contract_to_Clause__c 
                                where Contract__c =:cId order 
                                by createddate desc limit 50];
        return contractToClauseList;
      }
    
    public pagereference Save(){      
		upsert cds;
        return null;
      }
    
    public pagereference Cancel(){
         String cId= ApexPages.currentPage().getParameters().get('refRecord');
         Pagereference pg = new Pagereference('/apex/ContractDetailPage?id='+cId);
         pg.setredirect(true);
         return pg;
    }
}